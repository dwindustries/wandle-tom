var gulp = require("gulp"),
  rollup = require("gulp-better-rollup"),
  babel = require("rollup-plugin-babel"),
  minify = require("gulp-babel-minify"),
  resolve = require("rollup-plugin-node-resolve"),
  commonjs = require("rollup-plugin-commonjs"),
  concat = require("gulp-concat"),
  sourcemaps = require("gulp-sourcemaps"),
  plumber = require("gulp-plumber"),
  sass = require("gulp-ruby-sass"),
  postcss = require("gulp-postcss"),
  autoprefixer = require("autoprefixer"),
  livereload = require("gulp-livereload"),
  imageop = require("gulp-image-optimization"),
  svgSprite = require("gulp-svg-sprite"),
  gutil = require("gulp-util"),
  browserSync = require("browser-sync"),
  reload = browserSync.reload;

// Paths
var assets_path = "./wp-content/themes/wandle/assets";
var basePaths = {
  src: assets_path,
  dest: assets_path + "/build"
};
var paths = {
  images: {
    src: basePaths.src + "/images/"
  },
  sprite: {
    src: basePaths.src + "/images/svgs/*",
    svg: basePaths.dest + "/images/svgs/sprite.svg",
    css: basePaths.src + "/scss/_sprite.scss"
  },
  templates: {
    src: basePaths.src + "/build/tpl/"
  }
};

// Static server
gulp.task("browser-sync", function() {
  //initialize browsersync
  browserSync({
    proxy: "wandle-tom.l"
  });
});

function errorLog(error) {
  console.error.bind(error);
  this.emit("end");
}

// Builds and Adds Photoswipe JS Libs
gulp.task("lib-scripts", function() {
  gulp
    .src([
      assets_path + "/js/libs/ScrollMagic.min.js",
      assets_path + "/js/libs/TweenMax.min.js",
      assets_path + "/js/libs/ScrollToPlugin.min.js"
    ])
    .pipe(plumber())
    .pipe(concat("lib-scripts.js"))
    .pipe(gulp.dest(assets_path + "/build/js/"));
});

// Scripts
// uglifies and concatenates js
gulp.task("scripts", function() {
  var stream = gulp
    .src([assets_path + "/js/global.js"])
    .pipe(sourcemaps.init())
    .pipe(
      rollup(
        {
          plugins: [
            resolve({
              jsnext: true,
              main: true,
              browser: true
            }),
            commonjs(),
            babel({
              exclude: "node_modules/**",
              presets: ["@babel/preset-env"]
            })
          ]
        },
        "iife"
      )
    )
    .pipe(plumber())
    .pipe(
      minify({
        mangle: {
          keepClassNames: true
        }
      })
    )
    .pipe(concat("all.js"))
    .pipe(sourcemaps.write("."))
    .pipe(gulp.dest(assets_path + "/build/js/"));
  return stream;
});

// Styles
// uglifies, uses plumber for errors and live reload
gulp.task("styles", function() {
  var stream = sass(assets_path + "/scss/all.scss", {
    style: "compressed"
  })
    .on("error", function(err) {
      console.error("Error", err.message);
    })
    .pipe(sourcemaps.init())
    .pipe(postcss([autoprefixer({ browsers: ["last 4 versions"] })]))
    .pipe(sourcemaps.write("."))
    .pipe(gulp.dest(assets_path + "/build/css/"));
  return stream;
});

// SVG Sprite
// Create an SVG sprite wth png fallbacks
gulp.task("svgSprite", function() {
  var stream = gulp
    .src(paths.sprite.src)
    .pipe(
      svgSprite({
        mode: {
          css: {
            spacing: {
              padding: 5
            },
            dest: "./",
            layout: "diagonal",
            sprite: "../../assets/build/images/svgs/sprite.svg",
            bust: false,
            render: {
              scss: {
                dest: "../scss/_sprite.scss",
                template: assets_path + "/build/tpl/sprite-template.scss"
              }
            }
          }
        }
      })
    )
    .pipe(gulp.dest(basePaths.dest));
  return stream;
});

// Watch
gulp.task("watch", function() {
  livereload.listen();
  gulp
    .watch(assets_path + "/js/**/*.js", ["scripts"])
    .on("change", livereload.changed);
  gulp
    .watch(assets_path + "/scss/**/*.scss", ["styles"])
    .on("change", livereload.changed);
  gulp.watch("./**/*.php").on("change", livereload.changed);
});

// gulp Default
gulp.task("default", [
  "styles",
  "lib-scripts",
  "scripts",
  "svgSprite",
  "browser-sync",
  "watch"
]);
