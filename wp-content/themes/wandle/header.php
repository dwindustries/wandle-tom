<?php

/**
 * The Header for our theme
 *
 * Displays the <head> and <header> sections
 *
 * @package WordPress
 * @subpackage Wandle - Target Operating Model
 * @since Wandle - Target Operating Model 1.0
 */
?>

<!doctype html>
<html class="no-js" lang="">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Wandle - Target Operating Model</title>
<meta name="description" content="">
<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">

<link rel="stylesheet" href="https://use.typekit.net/kmr1aaw.css">
<link rel="icon" href="<?php echo get_stylesheet_directory_uri() . '/assets/images/icons/favicon.ico'; ?>" type="image/x-icon" />

<script type="text/javascript">
  // Apply js class to html
  document.documentElement.className = 'js';
</script>
<style type="text/css">
  /*Hide nav on page load if js available*/
  /* .js .site-header { display:none; }  */
</style>

<!--[if lt IE 9]>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script>window.html5 || document.write('<script src="js/vendor/html5shiv.js"><\/script>')</script>
	<![endif]-->

<?php wp_head(); ?>
</head>

<body>

  <div id="js-toolbar" class="toolbar">
    <div class="container-fluid">
      <div class="row">
        <div class="col-bp1-12">
          <div class="toolbar__inner">
            <span class="toolbar__line"></span>

            <a class="js-modal-trigger toolbar__video-trigger" href="">
              <svg width="67.06px" height="67.05px" viewBox="0 0 67.06 67.05" xml:space="preserve">
                <path style="fill:#353451;" d="M54.023,25.908l11.121-3.591C60.625,9.565,48.609,0.36,34.396,0v11.686
                                    C43.413,12.037,51.027,17.849,54.023,25.908" />
                <path style="fill:#464564;" d="M65.712,24.08L54.593,27.67c0.517,1.864,0.795,3.827,0.795,5.855c0,6.938-3.233,13.12-8.274,17.124
                                    l6.882,9.433C61.942,53.951,67.06,44.333,67.06,33.52C67.06,30.243,66.589,27.075,65.712,24.08" />
                <path style="fill:#B7B6D1;" d="M32.543,11.691V0.004C18.371,0.415,6.403,9.617,1.906,22.349l11.119,3.592
                                    C16,17.901,23.567,12.09,32.543,11.691" />
                <path style="fill:#8384A6;" d="M11.673,33.526c0-2.016,0.274-3.967,0.785-5.821l-11.12-3.593C0.468,27.097,0,30.254,0,33.52
                                    c0,10.742,5.052,20.304,12.908,26.441l6.911-9.414C14.852,46.541,11.673,40.405,11.673,33.526" />
                <path style="fill:#616180;" d="M21.309,51.649l-6.909,9.411c5.426,3.777,12.019,5.99,19.131,5.99c7.041,0,13.574-2.17,18.968-5.878
                                    l-6.882-9.434c-3.462,2.303-7.618,3.645-12.086,3.645C29.002,55.383,24.796,54.006,21.309,51.649" />
                <path style="fill:none;stroke:#FFFFFF;stroke-miterlimit:10;" d="M25.897,22.917v21.691c0,1.764,2.007,2.776,3.427,1.729
                                    l14.695-10.845c1.166-0.859,1.166-2.6,0-3.46L29.324,21.187C27.904,20.139,25.897,21.152,25.897,22.917z" />
              </svg>
              <span>Watch</span>
            </a>

            <a href="/" class="toolbar__logo">
              <img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/logos/wandle.svg'; ?>" alt="Wandle" />
            </a>

            <a class="js-nav-trigger site-nav-trigger">
              <span class="site-nav-trigger__text">Menu</span>
              <span class="lines-button x">
                <span class="lines"></span>
              </span>

              <svg width="67px" height="67px" viewBox="0 0 67 67" style="enable-background:new 0 0 67 67;" xml:space="preserve">
                <g>
                  <path style="fill:#FCC211;" d="M53.979,25.89l11.114-3.588C60.575,9.564,48.573,0.363,34.362,0v11.683
                                        C43.382,12.03,50.992,17.835,53.979,25.89z" />
                  <path style="fill:#EF432D;" d="M65.658,24.064L54.55,27.651c0.521,1.855,0.785,3.827,0.785,5.843
                                        c0,6.936-3.226,13.113-8.263,17.109l6.875,9.422C61.893,53.902,67,44.282,67,33.486C67.013,30.216,66.541,27.049,65.658,24.064z" />
                  <path style="fill:#47AF4A;" d="M32.518,11.683V0C18.361,0.412,6.393,9.602,1.894,22.325l11.116,3.587
                                        C15.989,17.889,23.553,12.083,32.518,11.683z" />
                  <path style="fill:#448AC9;" d="M11.668,33.494c0-2.016,0.259-3.965,0.785-5.812L1.346,24.095C0.468,27.072,0,30.227,0,33.494
                                        C0,44.229,5.052,53.78,12.903,59.91l6.908-9.392C14.84,46.507,11.668,40.371,11.668,33.494z" />
                  <path style="fill:#EB1B62;" d="M21.297,51.614l-6.907,9.403C19.817,64.788,26.396,67,33.503,67c7.035,0,13.561-2.167,18.959-5.873
                                        l-6.877-9.419c-3.454,2.294-7.617,3.629-12.082,3.629C28.98,55.337,24.78,53.969,21.297,51.614z" />
                  <g>
                    <g>
                      <line style="fill:none;stroke:#FFFFFF;stroke-miterlimit:10;" x1="33.503" y1="16.749" x2="33.503" y2="23.084" />
                      <line style="fill:none;stroke:#FFFFFF;stroke-miterlimit:10;" x1="33.503" y1="43.914" x2="33.503" y2="50.241" />
                    </g>
                    <g>
                      <line style="fill:none;stroke:#FFFFFF;stroke-miterlimit:10;" x1="50.259" y1="33.494" x2="43.932" y2="33.494" />
                      <line style="fill:none;stroke:#FFFFFF;stroke-miterlimit:10;" x1="23.088" y1="33.494" x2="16.76" y2="33.494" />
                    </g>
                  </g>
                </g>
              </svg>

            </a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <header id="site-header" class="site-header">
    <nav class="site-nav">
      <ul id="js-nav-items" class="nav-primary">
        <li class="menu-item menu-item--organisation"><a href="#organisation">Organisation</a></li>
        <li class="menu-item menu-item--people-management"><a href="#people-management">People Management</a></li>
        <li class="menu-item menu-item--process"><a href="#our-customers">Our Customers</a></li>
        <li class="menu-item menu-item--technology"><a href="#technology">Technology</a></li>
        <li class="menu-item menu-item--partners-and-alliances"><a href="#partners-and-alliances">Partners &amp; Alliances</a></li>
        <li class="menu-item menu-item--kpis-intro"><a href="#kpis-intro">KPIs</a></li>
      </ul>
    </nav>

    <svg class="site-header__circle" viewBox="0 0 1230.6 1231.45" style="enable-background:new 0 0 1230.6 1231.45;" xml:space="preserve">
      <circle cx="615.3" cy="615.73" r="566.44" />
      <circle cx="615.3" cy="615.73" r="407.88" />
    </svg>
  </header>