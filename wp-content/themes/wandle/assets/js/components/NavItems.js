import { changeTriggerText } from '../libs/utils'

// Control the Navigation page scrolling
function NavItems() {
    var DOM = {}

    function cacheDOM(){
        DOM = {
            items: document.querySelectorAll('#js-nav-items li a'),
            triggers: document.querySelectorAll('.js-nav-trigger'),
            nav: document.getElementById('site-header')
        }
    }

    function bindEvents(){
        for(let i = 0; i < DOM.items.length; i++){
            DOM.items[i].addEventListener("click", function(event){
                event.preventDefault();
                handleNavClick(event.currentTarget);
            }, true);
        }
    }    

    function handleNavClick(target){
        render(target);      
    }

    function updateHistory(id){
        // if supported by the browser we update the URL
        if (window.history && window.history.pushState) {
            history.pushState("", document.title, id);
        } 
    }

    function render(target){
        const id = target.getAttribute('href');
        
        // Change trigger text
        for(let i = 0; i < DOM.triggers.length; i++){
            let trigger = DOM.triggers[i];
            trigger.classList.remove('active');
            changeTriggerText(trigger, 'Menu');
        }

        // scroll to section with TweenLite
        TweenMax.to(window, 1.2, {scrollTo: id});

        // close the nav overlay
        DOM.nav.classList.toggle('active');

        // Append section id to url 
        updateHistory(id);
    }

    function init(){
        cacheDOM();
        bindEvents();
    }

    return { init }
}

export default new NavItems