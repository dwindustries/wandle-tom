function ToolbarAnimate(){
    // Setup the ScrollMagic controller
    const controller = new ScrollMagic.Controller();
    
    // global var for DOM els
    var DOM = {}

    function cacheDOM(){
        DOM = {
            section: document.getElementById("js-main"),
            toolbar: document.getElementById('js-toolbar')
        }
    }

    // build scene
    function setupScene(){
        let scene = new ScrollMagic.Scene({
            triggerElement: DOM.section, 
            triggerHook: 'onEnter', 
            offset: 800,
        })
        .addTo(controller);

        scene.on('enter', function(){
            toggleActive();
        });
    
        scene.on('leave', function(){
            toggleActive(true);
        });
    }

    function toggleActive(deactivate){
        if(deactivate){
            DOM.toolbar.classList.remove('active');
            DOM.toolbar.classList.remove('active-main');
        } else {
            DOM.toolbar.classList.add('active');
            DOM.toolbar.classList.add('active-main');
        }
    }

    function init(){
        cacheDOM();
        setupScene();
    }

    return { init }
}

export default new ToolbarAnimate