import { changeTriggerText } from "../libs/utils"

function NavToggle() {
    var DOM = {};

    // cache DOM elements
    function cacheDOM(){
        DOM = {
            triggers: document.querySelectorAll('.js-nav-trigger'),
            nav: document.getElementById('site-header'),
            toolbar: document.getElementById('js-toolbar')
        }
    }

    // Bind the click event to trigger
    function bindEvents(){
        for(let i = 0; i < DOM.triggers.length; i++){
            DOM.triggers[i].addEventListener("click", (event) => {
                event.preventDefault();
                handleMenuTrigger();
            }, false);
        }
    }
    
    // Handle the trigger click (just render class toggling)
    function handleMenuTrigger() {
        render();
    }

    // Render our DOM manipulation
    function render(){
        DOM.nav.classList.toggle('active');

        // Toggle Trigger Text 
        setTimeout(() => {
            for(let i = 0; i < DOM.triggers.length; i++){
                DOM.triggers[i].classList.toggle('active');
    
                if(DOM.triggers[i].classList.contains('active')){
                    changeTriggerText(DOM.triggers[i], 'Close');
                } else {
                    changeTriggerText(DOM.triggers[i], 'Menu');
                }
            }
        }, 100);

        // Toggle toolbar header active class
        if(!DOM.toolbar.classList.contains('active-main')){
            DOM.toolbar.classList.toggle('active');
        }
    }

    // Init method to call
    function init(){
        cacheDOM();
        bindEvents();
    }

    // Return our init method to kick us off
    return { init }
}

export default new NavToggle