import { endVideo, toggleOverflow } from '../libs/utils'

function Modal() {
    var DOM = {}

    function cacheDOM(){
        DOM = {
            modal: document.querySelector('.js-modal'),
            modalTriggers: document.querySelectorAll('.js-modal-trigger'),
            toolbar: document.getElementById('js-toolbar')    
        }
    }

    function bindEvents(){
        // Open modal via triggers
        for(let i = 0; i < DOM.modalTriggers.length; i++){
            DOM.modalTriggers[i].addEventListener("click", (event) => {
                event.preventDefault();    
                handleModalTrigger();
            }, true);
        }

        // Close modal via X icon
        const modalClose = DOM.modal.querySelector(".js-modal-close");
        modalClose.addEventListener("click", () => {
            event.preventDefault();
            closeModal();
        });        

        // Close modal via ESC key
        document.onkeydown = function(evt) {
            if(DOM.modal.classList.contains('active')){
                evt = evt || window.event;
                if (evt.keyCode == 27) { closeModal(); }
            }
        };      
    }

    function handleModalTrigger(){
        if(DOM.toolbar.classList.contains('active')){ 
            DOM.toolbar.classList.remove('active'); 
        }
        DOM.modal.classList.add('active');
        toggleOverflow();
    }

    function closeModal(){
        if(DOM.modal.classList.contains('active')){
            DOM.modal.classList.remove('active');
            toggleOverflow(true);
            endVideo(); // end the video
            if(DOM.toolbar.classList.contains('active-main')){ 
                DOM.toolbar.classList.add('active'); 
            }
        }
    }

    function init(){
        cacheDOM();
        bindEvents();
    }

    return { init }
}

export default new Modal
