function changeTriggerText(el, str){
    let text = el.querySelector('.site-nav-trigger__text');
    text.innerHTML = str;
    return text;
}

function endVideo(){
    // get existing YT video via it's iframe 
    const player = YT.get('js-video');
    player.stopVideo();
}

function toggleOverflow(disableOverflow){
    document.body.style.overflow = (disableOverflow) ? "auto" : "hidden";
} 

export { changeTriggerText, endVideo, toggleOverflow };