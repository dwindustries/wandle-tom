"use strict";

import NavToggle from './components/NavToggle'
import NavItems from './components/NavItems'
import ToolbarAnimate from './components/ToolbarAnimate'
import Modal from './components/Modal'

document.addEventListener("DOMContentLoaded", (event) => {     
    NavToggle.init()
    NavItems.init()
    ToolbarAnimate.init()
    Modal.init() 
}, true);