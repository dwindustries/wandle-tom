<?php

    /**
     * The default template for a page.
     *
     * @package     WordPress
     * @subpackage  Wandle - Target Operating Model
     * @since       Wandle - Target Operating Model 1.0
     */

    get_header();
?>

<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : ?>
        <?php the_post(); ?>
        <?php the_content(); ?>
    <?php endwhile ?>
<?php endif ?>

<?php get_footer(); ?>