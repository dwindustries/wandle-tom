<?php

/*
 * Template Name: Style Guide
*/

    get_header();
?>

<main class="main">

    <section class="section">
        <div class="container-fluid">
            <div class="row">
                <div class="col-bp1-12">
                                    
                    <div class="intro">
                        <h2>Lorem ipsum dolor sit amet consectetuer adipiscing elit</h2>  
                        <p>Wandle had a fit for purpose organisational structure and governance arrangements aligned to meet the needs of its customers, enabling delivery of consistently good services and value for money to our customers</p>
                    </div>

                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa 
                    <strong>strong</strong>. Cum sociis natoque penatibus 
                    et magnis dis parturient montes, nascetur ridiculus 
                    mus. Donec quam felis, ultricies nec, pellentesque 
                    eu, pretium quis, sem. Nulla consequat massa quis 
                    enim. Donec pede justo, fringilla vel, aliquet nec, 
                    vulputate eget, arcu. In enim justo, rhoncus ut, 
                    imperdiet a, venenatis vitae, justo. Nullam dictum 
                    felis eu pede <a class="external ext" href="#">link</a> 
                    mollis pretium. Integer tincidunt. Cras dapibus. 
                    Vivamus elementum semper nisi. Aenean vulputate 
                    eleifend tellus. Aenean leo ligula, porttitor eu, 
                    consequat vitae, eleifend ac, enim. Aliquam lorem ante, 
                    dapibus in, viverra quis, feugiat a, tellus. Phasellus 
                    viverra nulla ut metus varius laoreet. Quisque rutrum. 
                    Aenean imperdiet. Etiam ultricies nisi vel augue. 
                    Curabitur ullamcorper ultricies nisi.</p>
                    
                    <h2>Aenean commodo ligula eget dolor aenean massa</h2>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing 
                    elit. Aenean commodo ligula eget dolor. Aenean massa. 
                    Cum sociis natoque penatibus et magnis dis parturient 
                    montes, nascetur ridiculus mus. Donec quam felis, 
                    ultricies nec, pellentesque eu, pretium quis, sem.</p>
                    <h2>Aenean commodo ligula eget dolor aenean massa</h2>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing 
                    elit. Aenean commodo ligula eget dolor. Aenean massa. 
                    Cum sociis natoque penatibus et magnis dis parturient 
                    montes, nascetur ridiculus mus. Donec quam felis, 
                    ultricies nec, pellentesque eu, pretium quis, sem.</p>
                    <ul>
                    <li>Lorem ipsum dolor sit amet consectetuer.</li>
                    <li>Aenean commodo ligula eget dolor.</li>
                    <li>Aenean massa cum sociis natoque penatibus.</li>
                    </ul>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing 
                    elit. Aenean commodo ligula eget dolor. Aenean massa. 
                    Cum sociis natoque penatibus et magnis dis parturient 
                    montes, nascetur ridiculus mus. Donec quam felis, 
                    ultricies nec, pellentesque eu, pretium quis, sem.</p>

                    <table class="table--content">
                        <thead>
                            <tr>
                                <th>Entry Header 1</th>
                                <th>Entry Header 2</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td data-th="Entry Header 1"><p>Lorem ipsum dolor sit amet, consectetuer adipiscing 
                    elit. Aenean commodo ligula eget dolor.</p></td>
                                <td data-th="Entry Header 2"><p>Lorem ipsum dolor sit amet, consectetuer adipiscing 
                    elit. Aenean commodo ligula eget dolor. Aenean massa. 
                    Cum sociis natoque penatibus et magnis dis parturient 
                    montes, nascetur ridiculus mus. Donec quam felis, 
                    ultricies nec, pellentesque eu, pretium quis, sem.</p></td>
                            </tr>
                            <tr>
                                <td data-th="Entry Header 1"><p>Lorem ipsum dolor sit amet, consectetuer adipiscing 
                    elit. Aenean commodo ligula eget dolor. </p></td>
                                <td data-th="Entry Header 2"><p>Lorem ipsum dolor sit amet, consectetuer adipiscing 
                    elit. Aenean commodo ligula eget dolor. Aenean massa. 
                    Cum sociis natoque penatibus et magnis dis parturient 
                    montes, nascetur ridiculus mus. Donec quam felis, 
                    ultricies nec, pellentesque eu, pretium quis, sem.</p></td>
                            </tr>
                        </tbody>
                    </table>

                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing 
                    elit. Aenean commodo ligula eget dolor. Aenean massa. 
                    Cum sociis natoque penatibus et magnis dis parturient 
                    montes, nascetur ridiculus mus. Donec quam felis, 
                    ultricies nec, pellentesque eu, pretium quis, sem.</p>
                    
                    <form action="#" method="post">
                        <fieldset>
                            <div class="form__group">
                                <label for="name">Name</label>
                                <input type="text" id="name" placeholder="Enter your full name" />
                            </div>
                            <div class="form__group">
                                <label for="email">Email</label>
                                <input type="email" id="email" placeholder="Enter your email address" />
                            </div>
                            <div class="form__group">
                                <label for="message">Message</label>
                                <textarea id="message" placeholder="What's on your mind?"></textarea>
                            </div>
                            <button class="button" type="submit">Submit</button>
                        </fieldset>
                    </form>

                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing 
                    elit. Aenean commodo ligula eget dolor. Aenean massa. 
                    Cum sociis natoque penatibus et magnis dis parturient 
                    montes, nascetur ridiculus mus. Donec quam felis, 
                    ultricies nec, pellentesque eu, pretium quis, sem.</p>
                   
                    <table class="table--striped">
                        <thead>
                            <tr>
                                <th>Entry Header 1</th>
                                <th>Entry Header 2</th>
                                <th>Entry Header 3</th>
                                <th>Entry Header 4</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td data-th="Entry Header 1">Entry First Line 1</td>
                                <td data-th="Entry Header 2">Entry First Line 2</td>
                                <td data-th="Entry Header 3">Entry First Line 3</td>
                                <td data-th="Entry Header 4">Entry First Line 4</td>
                            </tr>
                            <tr>
                                <td data-th="Entry Header 1">Entry First Line 1</td>
                                <td data-th="Entry Header 2">Entry First Line 2</td>
                                <td data-th="Entry Header 3">Entry First Line 3</td>
                                <td data-th="Entry Header 4">Entry First Line 4</td>
                            </tr>
                            <tr>
                                <td data-th="Entry Header 1">Entry First Line 1</td>
                                <td data-th="Entry Header 2">Entry First Line 2</td>
                                <td data-th="Entry Header 3">Entry First Line 3</td>
                                <td data-th="Entry Header 4">Entry First Line 4</td>
                            </tr>
                        </tbody>
                    </table>

                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing 
                    elit. Aenean commodo ligula eget dolor. Aenean massa. 
                    Cum sociis natoque penatibus et magnis dis parturient 
                    montes, nascetur ridiculus mus. Donec quam felis, 
                    ultricies nec, pellentesque eu, pretium quis, sem.</p>

                </div>
            </div>
        </div>
    </section>

</main>

<?php get_footer(); ?>