<?php

/*
 * Template Name: Home
*/

get_header();
?>

<?php get_template_part('elements/components/hero'); ?>

<main id="js-main" class="main">
    <?php if( have_rows('sections') ) : ?>
        <?php while ( have_rows('sections') ) : ?>
            <?php the_row(); ?>

            <section id="<?php echo get_sub_field('section_anchor') ?>" class="section section--space">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-bp1-12">

                            <div class="intro intro--<?php echo get_sub_field('section_anchor') ?>">
                                <h2 class="intro__title"><?php echo get_sub_field('section_title') ?></h2>
                                <div class="row">
                                    <div class="col-bp1-12 col-bp4-7 col-bp5-8">
                                        <?php echo get_sub_field('section_intro') ?>
                                    </div>
                                    <div class="col-bp1-12 col-bp4-5 col-bp5-4">
                                        <div class="intro__icons">
                                            <a href="<?php echo get_sub_field('section_link') ?>" class="intro__icon intro__icon--link">
                                                <span>More info</span>
                                            </a>
                                            <?php if(get_sub_field('section_icon')) : ?>
                                                <div class="intro__icon intro__icon--symbol">
                                                    <?php echo get_sub_field('section_icon') ?>
                                                </div>
                                            <?php endif ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php if( have_rows('section_table') ) : ?>
                                <table class="table--content">
                                    <thead>
                                        <tr>
                                            <th>Our vision</th>
                                            <th>What outcome are we trying to achieve?</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php while ( have_rows('section_table') ) : ?>
                                            <?php the_row(); ?>                         
                                            <tr>
                                                <td data-th="Our vision">
                                                    <p><?php echo get_sub_field('section_table_col_1') ?></p>
                                                </td>
                                                <td data-th="What outcome are we trying to achieve?">
                                                    <?php echo get_sub_field('section_table_col_2') ?>
                                                </td>
                                            </tr>
                                        <?php endwhile ?>
                                        
                                    </tbody>
                                </table>
                            <?php endif ?>                                          
                                
                        </div>
                    </div>
                </div>
            </section>

            <section class="section section--<?php echo get_sub_field('section_anchor') ?>">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-bp1-12">
                            <h2 class="section__title"><?php echo get_sub_field('section_quotation_title') ?></h2>
                        </div>
                    </div>
                    <div class="row bottom-bp4">
                        <div class="col-bp1-12 col-bp4-8 col-bp5-9">
                            <div class="block block--bordered">
                                <?php echo get_sub_field('section_quotation') ?>
                            </div>
                        </div>
                        <div class="col-bp1-12 col-bp4-4 col-bp5-3">
                            <blockquote class="pullquote">
                                <?php echo get_sub_field('section_pull_quote') ?>
                            </blockquote>
                        </div>
                    </div>
                </div>
            </section>

        <?php endwhile ?>
    <?php endif ?>     
    
    <?php if( have_rows('kpis') ) : ?>
    <section id="kpis" class="section section--space ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-bp1-12">

                    <div class="intro">
                        <h2 class="intro__title"><?php echo get_field('section_title_kpis') ?></h2>
                        <div class="row">
                            <div class="col-bp1-12 col-bp3-8">
                                <?php echo get_field('section_intro_kpis') ?>
                            </div>
                            <div class="col-bp1-12 col-bp3-4">
                                <div class="intro__icons">    
                                    <div class="intro__icon intro__icon--symbol intro__icon--kpis">
                                        <?php echo get_field('section_icon_kpis') ?>
                                    </div>
                                </div>
                            </div>
                        </div>   
                    </div>             
                    
                    <table class="table--striped">
                        <thead>
                            <tr>
                                <th>Service</th>
                                <th>Measure</th>
                                <th></th>
                                <th>Year 1</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php while ( have_rows('kpis') ) : ?>
                                <?php the_row(); ?>
                                <tr>
                                    <td data-th="Service"><?php echo get_sub_field('kpis_service') ?></td>
                                    <td data-th="Measure"><?php echo get_sub_field('kpis_measure') ?></td>
                                    <td data-th="Status"><span class="td-status td-status--<?php echo get_sub_field('kpis_status') ?>"></span></td>
                                    <td data-th="Year 1"><?php echo get_sub_field('kpis_year_1') ?></td>
                                </tr>
                            <?php endwhile ?>
                        </tbody>
                    </table>

                  </div>
              </div>
          </div>
      </section>
    <?php endif ?>
</main>

<?php get_footer(); ?>