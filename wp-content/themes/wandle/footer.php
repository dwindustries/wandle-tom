<footer class="site-footer">
<div class="container-fluid">
        <div class="row">
            <div class="col-bp1-12">

                <div class="site-footer__inner">
                    <img class="site-footer__brand" src="<?php echo get_stylesheet_directory_uri() . '/assets/images/logos/wandle.svg'; ?>" alt="Wandle" />

                    <div class="site-footer__desc">
                        <p>Registered oﬃce</p>
                        <p>Second Floor, Minerva House, Montague Close, London, SE1 9BB</p>
                        <p>Registered Provider no. L0277  Charitable Registered Society no. 19225R</p>
                    </div>

                    <div class="site-footer__detail">
                        <p>Go to the <a href="https://www.wandle.com/" target="blank">wandle.com</a></p>
                    </div>

                    <div class="site-footer__detail">
                        <p><a href="#top">Return to top</a></p>
                    </div>

                    <div class="site-footer__detail">
                        <p>Website by <a href="http://broadgatecreative.co.uk" target="blank">Broadgate Creative</a></p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</footer>

<script>
    // Youtube Iframe Embed (Must be in global scope)
    // This code loads the IFrame Player API code asynchronously.
    createScriptTag('https://www.youtube.com/iframe_api');  

    // YT function - call this function once
    function onYouTubeIframeAPIReady() {
        const video = document.getElementById('js-video');
        const videoId = video.getAttribute('data-youtube-id');
        
        if(typeof video === 'undefined')
            return; 

        player = new YT.Player(video, {
            width: '1280',
            height: '720',
            videoId: videoId,
            playerVars: { 
                'rel': 0
            }, 
        });
    }

    // Create a Script tag using a src param
    function createScriptTag(src) {
        var tag = document.createElement('script');
        tag.setAttribute('src', src);
        var head = document.getElementsByTagName('head')[0];
        head.appendChild(tag);
    }
</script>

<?php wp_footer(); ?>

</body>
</html>