<?php
/**
 * Functions and definitions
 *
 * @package     WordPress
 * @subpackage  Wandle - Target Operating Model
 * @since       Wandle - Target Operating Model 1.0
 */

  /* Thumbnail support */
  add_theme_support('post-thumbnails');

  // Add new image sizes
  if ( function_exists( 'add_image_size' ) ) { 

  }

  // Create the Custom Primary Navigation
  require_once 'functions/tidy-wp-admin.php';

  
  // Register Nav Menu
  function register_menu() {
	register_nav_menu('primary-menu',__( 'Primary Menu' ));
  }
  add_action( 'init', 'register_menu' );
    

    /* ======================*
   *  Scripts & Styles     *
	 * ======================*/

    // Change Stylesheet Directory
    function stylesheet_dir() {

        wp_register_style( 'all', get_stylesheet_directory_uri().'/assets/build/css/all.css' );
        wp_enqueue_style( 'all' );

    }
    add_action( 'wp_enqueue_scripts', 'stylesheet_dir' );

    /* Add scripts via wp_head() */
    function script_enqueuer() {

        wp_register_script( 'lib-scripts', get_template_directory_uri().'/assets/build/js/lib-scripts.js','','1.0', true );   
        wp_register_script( 'all-js', get_template_directory_uri().'/assets/build/js/all.js','','1.0', true );   

        wp_enqueue_script( 'lib-scripts' );
        wp_enqueue_script( 'all-js' );

    }
    add_action( 'wp_enqueue_scripts', 'script_enqueuer' );