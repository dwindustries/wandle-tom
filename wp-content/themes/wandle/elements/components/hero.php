<div id="top" class="hero">
  <div class="container-fluid">
    <div class="row">
      <div class="col-bp1-12">
        <div class="hero__inner">
          <header class="hero__header">
            <img class="hero__brand" src="<?php echo get_stylesheet_directory_uri() . '/assets/images/logos/wandle.svg'; ?>" alt="Wandle" />
            <h2 class="hero__slogan">Target Operating Model <span>V0.1</span></h2>
          </header>

          <main class="hero__main">
            <div class="l-hero-main-col">
              <div class="hero__intro">
                <p>Wandle's value proposition is to be customer and community focussed, offering value for money, good reliable services and safe and affordable homes.  We do this through the following six strategic aims:</p>
              </div>
            </div>
            <div class="l-hero-main-col">
              <div class="chart">
                <svg class="chart__circle chart__circle1" viewBox="0 0 557 559" xml:space="preserve">
                  <circle cx="278.5" cy="279.5" r="255.84" />
                  <circle cx="278.5" cy="279.5" r="176.84" />
                </svg>

                <svg class="chart__circle chart__circle2" viewBox="0 0 1230.6 1231.45" xml:space="preserve">
                  <circle cx="615.3" cy="615.73" r="566.44" />
                  <circle cx="615.3" cy="615.73" r="407.88" />
                </svg>

                <svg class="chart__circle chart__circle3" viewBox="0 0 578 574" xml:space="preserve">
                  <circle cx="289" cy="287" r="270.51" />
                  <circle cx="289" cy="287" r="191.46" />
                </svg>

                <?php get_template_part('elements/components/chart-nav'); ?>
              </div>
            </div>
            <div class="l-hero-main-col">
              <div class="js-modal-trigger hero__main-video-trigger">
                <svg width="80px" height="80px" viewBox="0 0 67.06 67.05" xml:space="preserve">
                  <path style="fill:#353451;" d="M54.023,25.908l11.121-3.591C60.625,9.565,48.609,0.36,34.396,0v11.686
                                        C43.413,12.037,51.027,17.849,54.023,25.908" />
                  <path style="fill:#464564;" d="M65.712,24.08L54.593,27.67c0.517,1.864,0.795,3.827,0.795,5.855c0,6.938-3.233,13.12-8.274,17.124
                                        l6.882,9.433C61.942,53.951,67.06,44.333,67.06,33.52C67.06,30.243,66.589,27.075,65.712,24.08" />
                  <path style="fill:#B7B6D1;" d="M32.543,11.691V0.004C18.371,0.415,6.403,9.617,1.906,22.349l11.119,3.592
                                        C16,17.901,23.567,12.09,32.543,11.691" />
                  <path style="fill:#8384A6;" d="M11.673,33.526c0-2.016,0.274-3.967,0.785-5.821l-11.12-3.593C0.468,27.097,0,30.254,0,33.52
                                        c0,10.742,5.052,20.304,12.908,26.441l6.911-9.414C14.852,46.541,11.673,40.405,11.673,33.526" />
                  <path style="fill:#616180;" d="M21.309,51.649l-6.909,9.411c5.426,3.777,12.019,5.99,19.131,5.99c7.041,0,13.574-2.17,18.968-5.878
                                        l-6.882-9.434c-3.462,2.303-7.618,3.645-12.086,3.645C29.002,55.383,24.796,54.006,21.309,51.649" />
                  <path style="fill:none;stroke:#FFFFFF;stroke-miterlimit:10;" d="M25.897,22.917v21.691c0,1.764,2.007,2.776,3.427,1.729
                                        l14.695-10.845c1.166-0.859,1.166-2.6,0-3.46L29.324,21.187C27.904,20.139,25.897,21.152,25.897,22.917z" />
                </svg>
                <p>Watch: our journey to 2024 explained</p>
              </div>
            </div>
          </main>

          <footer class="hero__footer">
            <span class="hero__line"></span>

            <a class="js-modal-trigger hero__video-trigger" href="">
              <svg width="67.06px" height="67.05px" viewBox="0 0 67.06 67.05" xml:space="preserve">
                <path style="fill:#353451;" d="M54.023,25.908l11.121-3.591C60.625,9.565,48.609,0.36,34.396,0v11.686
                                    C43.413,12.037,51.027,17.849,54.023,25.908" />
                <path style="fill:#464564;" d="M65.712,24.08L54.593,27.67c0.517,1.864,0.795,3.827,0.795,5.855c0,6.938-3.233,13.12-8.274,17.124
                                    l6.882,9.433C61.942,53.951,67.06,44.333,67.06,33.52C67.06,30.243,66.589,27.075,65.712,24.08" />
                <path style="fill:#B7B6D1;" d="M32.543,11.691V0.004C18.371,0.415,6.403,9.617,1.906,22.349l11.119,3.592
                                    C16,17.901,23.567,12.09,32.543,11.691" />
                <path style="fill:#8384A6;" d="M11.673,33.526c0-2.016,0.274-3.967,0.785-5.821l-11.12-3.593C0.468,27.097,0,30.254,0,33.52
                                    c0,10.742,5.052,20.304,12.908,26.441l6.911-9.414C14.852,46.541,11.673,40.405,11.673,33.526" />
                <path style="fill:#616180;" d="M21.309,51.649l-6.909,9.411c5.426,3.777,12.019,5.99,19.131,5.99c7.041,0,13.574-2.17,18.968-5.878
                                    l-6.882-9.434c-3.462,2.303-7.618,3.645-12.086,3.645C29.002,55.383,24.796,54.006,21.309,51.649" />
                <path style="fill:none;stroke:#FFFFFF;stroke-miterlimit:10;" d="M25.897,22.917v21.691c0,1.764,2.007,2.776,3.427,1.729
                                    l14.695-10.845c1.166-0.859,1.166-2.6,0-3.46L29.324,21.187C27.904,20.139,25.897,21.152,25.897,22.917z" />
              </svg>
              <span>Watch</span>
            </a>

            <a href="#organisation" class="hero__more" title="View More">
              <svg width="72.189px" height="72.189px" viewBox="0 0 72.189 72.189" xml:space="preserve">
                <circle style="fill:none;stroke:#FFFFFF;stroke-width:2;stroke-miterlimit:10;" cx="36.094" cy="36.094" r="35.094" />
                <line style="fill:none;stroke:#FFFFFF;stroke-width:2;stroke-miterlimit:10;" x1="36.093" y1="17.121" x2="36.093" y2="51.282" />
                <polygon style="fill:#FFFFFF;" points="27.914,44.655 29.378,43.292 36.091,50.515 42.806,43.292 44.27,44.655 36.091,53.452 " />
              </svg>
            </a>

            <a class="js-nav-trigger site-nav-trigger site-nav-trigger--hero">
              <span class="site-nav-trigger__text">Menu</span>
              <span class="lines-button x">
                <span class="lines"></span>
              </span>

              <svg width="67px" height="67px" viewBox="0 0 67 67" style="enable-background:new 0 0 67 67;" xml:space="preserve">
                <g>
                  <path style="fill:#FCC211;" d="M53.979,25.89l11.114-3.588C60.575,9.564,48.573,0.363,34.362,0v11.683
                                        C43.382,12.03,50.992,17.835,53.979,25.89z" />
                  <path style="fill:#EF432D;" d="M65.658,24.064L54.55,27.651c0.521,1.855,0.785,3.827,0.785,5.843
                                        c0,6.936-3.226,13.113-8.263,17.109l6.875,9.422C61.893,53.902,67,44.282,67,33.486C67.013,30.216,66.541,27.049,65.658,24.064z" />
                  <path style="fill:#47AF4A;" d="M32.518,11.683V0C18.361,0.412,6.393,9.602,1.894,22.325l11.116,3.587
                                        C15.989,17.889,23.553,12.083,32.518,11.683z" />
                  <path style="fill:#448AC9;" d="M11.668,33.494c0-2.016,0.259-3.965,0.785-5.812L1.346,24.095C0.468,27.072,0,30.227,0,33.494
                                        C0,44.229,5.052,53.78,12.903,59.91l6.908-9.392C14.84,46.507,11.668,40.371,11.668,33.494z" />
                  <path style="fill:#EB1B62;" d="M21.297,51.614l-6.907,9.403C19.817,64.788,26.396,67,33.503,67c7.035,0,13.561-2.167,18.959-5.873
                                        l-6.877-9.419c-3.454,2.294-7.617,3.629-12.082,3.629C28.98,55.337,24.78,53.969,21.297,51.614z" />
                  <g>
                    <g>
                      <line style="fill:none;stroke:#FFFFFF;stroke-miterlimit:10;" x1="33.503" y1="16.749" x2="33.503" y2="23.084" />
                      <line style="fill:none;stroke:#FFFFFF;stroke-miterlimit:10;" x1="33.503" y1="43.914" x2="33.503" y2="50.241" />
                    </g>
                    <g>
                      <line style="fill:none;stroke:#FFFFFF;stroke-miterlimit:10;" x1="50.259" y1="33.494" x2="43.932" y2="33.494" />
                      <line style="fill:none;stroke:#FFFFFF;stroke-miterlimit:10;" x1="23.088" y1="33.494" x2="16.76" y2="33.494" />
                    </g>
                  </g>
                </g>
              </svg>
            </a>
          </footer>

          <div class="js-modal modal">
            <a href="#" class="js-modal-close modal__close" title="Close video">
              <span>Close</span>
              <svg width="14.1px" height="14.1px" viewBox="17.7 9.1 14.1 14.1" xml:space="preserve">
                <path style="fill:#343351;" d="M29.1,14.5l2.3-0.8c-1-2.7-3.5-4.6-6.5-4.7v2.5C26.8,11.6,28.4,12.8,29.1,14.5z" />
                <path style="fill:#454463;" d="M31.5,14.1l-2.3,0.8c0.1,0.4,0.2,0.8,0.2,1.2c0,1.5-0.7,2.8-1.7,3.6l1.4,2
                                    c1.7-1.3,2.8-3.3,2.8-5.6C31.8,15.4,31.7,14.8,31.5,14.1z" />
                <path style="fill:#B7B6D1;" d="M24.5,11.5V9.1c-3,0.1-5.5,2-6.4,4.7l2.3,0.8C21.1,12.8,22.7,11.6,24.5,11.5z" />
                <path style="fill:#8383A5;" d="M20.1,16.1c0-0.4,0.1-0.8,0.2-1.2L18,14.1c-0.2,0.6-0.3,1.3-0.3,2c0,2.3,1.1,4.3,2.7,5.6l1.5-2
                                    C20.8,18.9,20.1,17.6,20.1,16.1z" />
                <path style="fill:#60607F;" d="M22.2,19.9l-1.5,2c1.1,0.8,2.5,1.3,4,1.3c1.5,0,2.9-0.5,4-1.2l-1.4-2c-0.7,0.5-1.6,0.8-2.5,0.8
                                    C23.8,20.7,22.9,20.4,22.2,19.9z" />
                <line style="fill:none;stroke:#FFFFFF;stroke-width:.3;stroke-linecap:round;stroke-miterlimit:10;" x1="22.5" y1="13.9" x2="27" y2="18.4" />
                <line style="fill:none;stroke:#FFFFFF;stroke-width:.3;stroke-linecap:round;stroke-miterlimit:10;" x1="27" y1="13.9" x2="22.5" y2="18.4" />
              </svg>
            </a>
            <div class="video-container">
              <div id="js-video" class="video" data-youtube-id="roBvSm_Yc8A"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>